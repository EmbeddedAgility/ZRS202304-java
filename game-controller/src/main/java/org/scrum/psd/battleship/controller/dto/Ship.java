package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    private boolean isPlaced;
    private String name;
    private int size;
    private List<Position> positions;
    private Color color;

    public Ship() {
        this.positions = new ArrayList<>();
    }

    public Ship(String name, int size) {
        this();

        this.name = name;
        this.size = size;
    }

    public Ship(String name, int size, List<Position> positions) {
        this(name, size);

        this.positions = positions;
    }

    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    public boolean addPosition(String input, Direction direction, List<Position> playingField) {
        if (positions == null) {
            positions = new ArrayList<>();
        }

        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));

        List<Position> shipPositions = new ArrayList<>();

        switch (direction) {
            case UP: {
                for (int i = 0; i < this.getSize(); i++) {
                    Position shipPosition = new Position(letter, number + i);
                    if (playingField.contains(shipPosition) && !playingField.get(playingField.indexOf(shipPosition)).isOccupied()) {
                        shipPosition.setOccupied(true);
                        shipPositions.add(shipPosition);
                    } else {
                        throw new RuntimeException();
                    }
                }
                positions.addAll(shipPositions);
                shipPositions.forEach(position -> playingField.set(playingField.indexOf(position), position));
                return true;
            }
            case DOWN: {
                for (int i = 0; i < this.getSize(); i++) {
                    Position shipPosition = new Position(letter, number - i);
                    if (playingField.contains(shipPosition) && !playingField.get(playingField.indexOf(shipPosition)).isOccupied()) {
                        shipPosition.setOccupied(true);
                        shipPositions.add(shipPosition);
                    } else {
                        throw new RuntimeException();
                    }
                }
                positions.addAll(shipPositions);
                shipPositions.forEach(position -> playingField.set(playingField.indexOf(position), position));
                return true;
            }
            case RIGHT: {
                for (int i = 0; i < this.getSize(); i++) {
                    Position shipPosition = new Position(letter.getNext(), number);
                    if (playingField.contains(shipPosition) && !playingField.get(playingField.indexOf(shipPosition)).isOccupied()) {
                        shipPosition.setOccupied(true);
                        shipPositions.add(shipPosition);
                    } else {
                        throw new RuntimeException();
                    }
                }
                positions.addAll(shipPositions);
                shipPositions.forEach(position -> playingField.set(playingField.indexOf(position), position));
                return true;
            }
            case LEFT: {
                for (int i = 0; i < this.getSize(); i++) {
                    Position shipPosition = new Position(letter.getPrevious(), number);
                    if (playingField.contains(shipPosition) && !playingField.get(playingField.indexOf(shipPosition)).isOccupied()) {
                        shipPosition.setOccupied(true);
                        shipPositions.add(shipPosition);
                    } else {
                        throw new RuntimeException();
                    }
                }
                positions.addAll(shipPositions);
                shipPositions.forEach(position -> playingField.set(playingField.indexOf(position), position));
                return true;
            }
            default: return false;
        }
    }

    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
