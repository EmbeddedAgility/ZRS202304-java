package org.scrum.psd.battleship.controller.dto;

public enum Letter {
    A, B, C, D, E, F, G, H;

    public Letter getNext() {
        switch (this) {
            case A: return B;
            case B: return C;
            case C: return D;
            case D: return E;
            case E: return F;
            case F: return G;
            case G: return H;
            default: throw new IllegalArgumentException();
        }
    }

    public Letter getPrevious() {
        switch (this) {
            case B: return A;
            case C: return B;
            case D: return C;
            case E: return D;
            case F: return E;
            case G: return F;
            case H: return G;
            default: throw new IllegalArgumentException();
        }
    }
}
