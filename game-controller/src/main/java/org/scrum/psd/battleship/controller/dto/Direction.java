package org.scrum.psd.battleship.controller.dto;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
