package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.dto.Direction;

public class DirectionFactory {

    public static Direction of(String input) {
        switch (input) {
            case "w": return Direction.UP;
            case "s": return Direction.DOWN;
            case "a": return Direction.LEFT;
            case "d": return Direction.RIGHT;
            default: throw new IllegalArgumentException();
        }
    }

}
