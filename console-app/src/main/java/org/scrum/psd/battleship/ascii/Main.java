package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Direction;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.*;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static List<Position> playingField;
    private static List<Position> playableFields;
    private static List<Ship> sunkenPlayerFleet;
    private static List<Ship> sunkenComputerFleet;
    private static int stepCounter = 1;
    private static Scanner scanner;

    private static final Telemetry telemetry = new Telemetry();

    public static void main(String[] args) {
        telemetry.trackEvent("ApplicationStarted", "Technology", "Java");
        System.out.println(colorize("                                     |__", MAGENTA_TEXT()));
        System.out.println(colorize("                                     |\\/", MAGENTA_TEXT()));
        System.out.println(colorize("                                     ---", MAGENTA_TEXT()));
        System.out.println(colorize("                                     / | [", MAGENTA_TEXT()));
        System.out.println(colorize("                              !      | |||", MAGENTA_TEXT()));
        System.out.println(colorize("                            _/|     _/|-++'", MAGENTA_TEXT()));
        System.out.println(colorize("                        +  +--|    |--|--|_ |-", MAGENTA_TEXT()));
        System.out.println(colorize("                     { /|__|  |/\\__|  |--- |||__/", MAGENTA_TEXT()));
        System.out.println(colorize("                    +---------------___[}-_===_.'____                 /\\", MAGENTA_TEXT()));
        System.out.println(colorize("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _", MAGENTA_TEXT()));
        System.out.println(colorize(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7", MAGENTA_TEXT()));
        System.out.println(colorize("|                        Welcome to Battleship                         BB-61/", MAGENTA_TEXT()));
        System.out.println(colorize(" \\_________________________________________________________________________|", MAGENTA_TEXT()));
        System.out.println("");

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        scanner = new Scanner(System.in);

        printWithColor(Colors.ANSI_CYAN, "                  __", true);
        printWithColor(Colors.ANSI_CYAN, "                 /  \\", true);
        printWithColor(Colors.ANSI_CYAN, "           .-.  |    |", true);
        printWithColor(Colors.ANSI_CYAN, "   *    _.-'  \\  \\__/", true);
        printWithColor(Colors.ANSI_CYAN, "    \\.-'       \\", true);
        printWithColor(Colors.ANSI_CYAN, "   /          _/", true);
        printWithColor(Colors.ANSI_CYAN, "  |      _  /\" \"", true);
        printWithColor(Colors.ANSI_CYAN, "  |     /_\'", true);
        printWithColor(Colors.ANSI_CYAN, "   \\    \\_/", true);
        printWithColor(Colors.ANSI_CYAN, "    \" \"\" \"\" \"\" \"", true);

        stepCounter = 1;
        printStepInfo("Shooting starts...");

        do {
            printStepInfo("Turn " + stepCounter++);
            System.out.println("");
            printWithColor(Colors.ANSI_CYAN, "Player, it's your turn", true);
            showShipStatuses();
            printWithColor(Colors.ANSI_CYAN, "Enter coordinates for your shot:", true);
            Position position = null;
            boolean isHit = false;
            do {
                System.out.println("");
                System.out.println("Player, it's your turn");
                System.out.println("Enter coordinates for your shot :");
                try {
                    String output = scanner.next();
                    if (output.equalsIgnoreCase("cheat")) {
                        endGame();
                    }
                    position = parsePosition(output);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid Letter entered.");
                    continue;
                }
                isHit = GameController.checkIsHit(enemyFleet, position);
                if (isHit) {
                    beep();

                    printWithColor(Colors.ANSI_BLUE, "                \\         .  ./", true);
                    printWithColor(Colors.ANSI_BLUE, "              \\      .:\" \";'.:..\" \"   /", true);
                    printWithColor(Colors.ANSI_BLUE, "                  (M^^.^~~:.'\" \").", true);
                    printWithColor(Colors.ANSI_BLUE, "            -   (/  .    . . \\ \\)  -", true);
                    printWithColor(Colors.ANSI_BLUE, "               ((| :. ~ ^  :. .|))", true);
                    printWithColor(Colors.ANSI_BLUE, "            -   (\\- |  \\ /  |  /)  -", true);
                    printWithColor(Colors.ANSI_BLUE, "                 -\\  \\     /  /-", true);
                    printWithColor(Colors.ANSI_BLUE, "                   \\  \\   /  /", true);
                    printWithColor(Colors.ANSI_BLUE, "Yeah ! Nice hit !", true);
                    Ship sunken = GameController.removeShip(enemyFleet);
                    if (sunken != null) {
                        sunkenComputerFleet.add(sunken);
                    }
                    GameController.removeShip(enemyFleet);
                } else {
                    printWithColor(Colors.ANSI_RED, "Miss", true);
                }

                telemetry.trackEvent("Player_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
                if (isOutsideField(position)) {
                    System.out.println("Shot outside of playing field, please try again.");
                }
            } while (isOutsideField(position));

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);
            System.out.println("");
            printWithColor(Colors.ANSI_CYAN, String.format("Computer shoot in %s and %s: ", position.getColumn(), position.getRow()), false);
            telemetry.trackEvent("Computer_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            if (isHit) {
                beep();

                printWithColor(Colors.ANSI_RED, "                \\         .  ./", true);
                printWithColor(Colors.ANSI_RED, "              \\      .:\" \";'.:..\" \"   /", true);
                printWithColor(Colors.ANSI_RED, "                  (M^^.^~~:.'\" \").", true);
                printWithColor(Colors.ANSI_RED, "            -   (/  .    . . \\ \\)  -", true);
                printWithColor(Colors.ANSI_RED, "               ((| :. ~ ^  :. .|))", true);
                printWithColor(Colors.ANSI_RED, "            -   (\\- |  \\ /  |  /)  -", true);
                printWithColor(Colors.ANSI_RED, "                 -\\  \\     /  /-", true);
                printWithColor(Colors.ANSI_RED, "                   \\  \\   /  /", true);
                printWithColor(Colors.ANSI_RED, "hit your ship !", true);
                Ship sunken = GameController.removeShip(myFleet);
                if (sunken != null) {
                    sunkenPlayerFleet.add(sunken);
                }
                GameController.removeShip(myFleet);

            } else {
                printWithColor(Colors.ANSI_BLUE, "miss", true);
            }
        } while (myFleet.size() > 0 && enemyFleet.size() > 0);

        endGame();
    }

    public static void printWithColor(String ansiRed, String s, boolean newLine) {
        if (newLine) {
            System.out.println(ansiRed + s + Colors.ANSI_RESET);
        } else {
            System.out.print(ansiRed + s + Colors.ANSI_RESET);
        }
    }

    private static boolean isOutsideField(Position position) {
        return !playingField.contains(position);
    }

    private static void beep() {
        System.out.print("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        Random random = new Random();
        int number = random.nextInt(playableFields.size()) - 1;
        Position removedPosition = playableFields.get(number);
        playableFields.remove(number);
        return removedPosition;
    }

    private static void InitializeGame() {
        InitializePlayingField();

        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        printStepInfo("Initialization of the fleet (player 1)");
        scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();
        sunkenPlayerFleet = new ArrayList<>();

        printWithColor(Colors.ANSI_CYAN, "Please position your fleet (Game board has size from A to H and 1 to 8) :", true);

        for (Ship ship : myFleet) {
            boolean result = false;
            do {
                System.out.println("");
                printWithColor(Colors.ANSI_CYAN, String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()), true);
                printWithColor(Colors.ANSI_CYAN, String.format("Enter position"), true);
                String positionInput = scanner.next();
                if (positionInput.equalsIgnoreCase("cheat")) {
                    endGame();
                }
                printWithColor(Colors.ANSI_CYAN, String.format("Enter direction"), true);
                String directionInput = scanner.next();
                if (directionInput.equalsIgnoreCase("cheat")) {
                    endGame();
                }
                Direction direction = DirectionFactory.of(directionInput);
                try {
                    result = ship.addPosition(positionInput, direction, playingField);
                    telemetry.trackEvent("Player_PlaceShipPosition", "Position", positionInput, "Ship", ship.getName());
                } catch (RuntimeException e) {
                    System.out.println("Invalid position, try again.");
                }
            } while (!result);
        }
    }

    public static void printStepInfo(String text) {
        printWithColor(Colors.ANSI_GREEN, text, true);
        printWithColor(Colors.ANSI_GREEN, "******  ******  ******  ******  ******  ******  ******  ", true);
        printWithColor(Colors.ANSI_GREEN, "*    *  *    *  *    *  *    *  *    *  *    *  *    *  ", true);
    }

    private static void InitializeEnemyFleet() {
        printStepInfo("Initialization of the fleet (computer)");
        enemyFleet = GameController.initializeShips();
        sunkenComputerFleet = new ArrayList<>();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 9));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static void InitializePlayingField() {
        playingField = new ArrayList<>();
        playingField.add(new Position(Letter.A, 0));
        playingField.add(new Position(Letter.A, 1));
        playingField.add(new Position(Letter.A, 2));
        playingField.add(new Position(Letter.A, 3));
        playingField.add(new Position(Letter.A, 4));
        playingField.add(new Position(Letter.A, 5));
        playingField.add(new Position(Letter.A, 6));
        playingField.add(new Position(Letter.A, 7));

        playingField.add(new Position(Letter.B, 0));
        playingField.add(new Position(Letter.B, 1));
        playingField.add(new Position(Letter.B, 2));
        playingField.add(new Position(Letter.B, 3));
        playingField.add(new Position(Letter.B, 4));
        playingField.add(new Position(Letter.B, 5));
        playingField.add(new Position(Letter.B, 6));
        playingField.add(new Position(Letter.B, 7));

        playingField.add(new Position(Letter.C, 0));
        playingField.add(new Position(Letter.C, 1));
        playingField.add(new Position(Letter.C, 2));
        playingField.add(new Position(Letter.C, 3));
        playingField.add(new Position(Letter.C, 4));
        playingField.add(new Position(Letter.C, 5));
        playingField.add(new Position(Letter.C, 6));
        playingField.add(new Position(Letter.C, 7));

        playingField.add(new Position(Letter.D, 0));
        playingField.add(new Position(Letter.D, 1));
        playingField.add(new Position(Letter.D, 2));
        playingField.add(new Position(Letter.D, 3));
        playingField.add(new Position(Letter.D, 4));
        playingField.add(new Position(Letter.D, 5));
        playingField.add(new Position(Letter.D, 6));
        playingField.add(new Position(Letter.D, 7));

        playingField.add(new Position(Letter.E, 0));
        playingField.add(new Position(Letter.E, 1));
        playingField.add(new Position(Letter.E, 2));
        playingField.add(new Position(Letter.E, 3));
        playingField.add(new Position(Letter.E, 4));
        playingField.add(new Position(Letter.E, 5));
        playingField.add(new Position(Letter.E, 6));
        playingField.add(new Position(Letter.E, 7));

        playingField.add(new Position(Letter.F, 0));
        playingField.add(new Position(Letter.F, 1));
        playingField.add(new Position(Letter.F, 2));
        playingField.add(new Position(Letter.F, 3));
        playingField.add(new Position(Letter.F, 4));
        playingField.add(new Position(Letter.F, 5));
        playingField.add(new Position(Letter.F, 6));
        playingField.add(new Position(Letter.F, 7));

        playingField.add(new Position(Letter.G, 0));
        playingField.add(new Position(Letter.G, 1));
        playingField.add(new Position(Letter.G, 2));
        playingField.add(new Position(Letter.G, 3));
        playingField.add(new Position(Letter.G, 4));
        playingField.add(new Position(Letter.G, 5));
        playingField.add(new Position(Letter.G, 6));
        playingField.add(new Position(Letter.G, 7));

        playingField.add(new Position(Letter.H, 0));
        playingField.add(new Position(Letter.H, 1));
        playingField.add(new Position(Letter.H, 2));
        playingField.add(new Position(Letter.H, 3));
        playingField.add(new Position(Letter.H, 4));
        playingField.add(new Position(Letter.H, 5));
        playingField.add(new Position(Letter.H, 6));
        playingField.add(new Position(Letter.H, 7));
        playableFields = new ArrayList<>(playingField);
    }

    public static void showShipStatuses() {
        if (sunkenComputerFleet.size() > 0) {
            printWithColor(Colors.ANSI_RED, "Sunken ships: ", true);
            System.out.println(" ");

            for (Ship ship : sunkenComputerFleet) {
                printWithColor(Colors.ANSI_RED, ship.getName() + " ", true);
            }
            System.out.println(" ");
        }
        if (enemyFleet.size() > 0) {
            printWithColor(Colors.ANSI_BLUE, "Enemy ships left: ", true);
            System.out.println(" ");
            for (Ship ship : enemyFleet) {
                printWithColor(Colors.ANSI_BLUE, ship.getName() + " ", true);
            }
        }
        System.out.println(" ");
    }

    private static void printArt() {
       String[] arts = new String[]{
               "                     |    |    |\n" +
               "                  )_)  )_)  )_)\n" +
               "               )___))___))___)\n" +
               "            )____)____)_____)\\\\\n" +
               "         _____|____|____|____\\\\\\\\\\\n" +
               "---------\\\\                   //^\\\\\\\\________\n" +
               "   ^^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^",
               "⠀⠀⣠⣤⣤⣤⣤⣤⣤⡄⠀⠀⠀⠀\n" +
               "⢀⡜⠉⢉⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀\n" +
               "⠘⣿⣷⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀\n" +
               "⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄\n" +
               "⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀\n" +
               "⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇\n" +
               "⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠃",
               "⠀⠀⠀⠀⢀⣀⣀⣀⣀⣀⣀⣀⣀⣤⣤⣶⣶⡶⠤⠶⣶⣶⣤⣀⣀⣀⣀⣀⣀⣀⣀⡀⠀⠀⠀⠀\n" +
               "⠀⠀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀\n" +
               "⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀\n" +
               "⠀⠈⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠛⠁⠀⠀\n" +
               "⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⡁⠀⠀⠀⠀⠀⠀⠀⠀\n" +
               "⠀⠀⠀⠀⠀⠀⢀⣿⣿⣿⣿⣷⣶⣤⡄⠀⠀⠀⠀⣠⣤⣶⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀\n" +
               "⠀⠀⠀⠀⠀⠀⢸⡿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀\n" +
               "⠀⠀⠀⠀⠀⠀⠘⣷⡹⢿⣿⣿⣿⣿⡿⢻⡇⠀⠀⠻⣿⣿⣿⣿⡿⢻⣇⠀⠀⠀⠀⠀⠀⠀⠀\n" +
               "⠀⠀⠀⠀⠀⠀⢀⣿⠃⠸⣿⣿⣿⡇⢸⡇⠀⠀⣠⣾⣿⣿⣿⣇⠙⣿⣿",
       };

       Random random = new Random();
       System.out.println(arts[random.nextInt(arts.length)]);
    }

    public static void endGame() {
        if (myFleet.size() > 0) {
            printWithColor(Colors.ANSI_GREEN, "  _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _\n" +
                    " / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\ / \\\n" +
                    "( Y | O | U |   | W | O | N |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |\n" +
                    " \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \\_/ \n", true);
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }

            printWithColor(Colors.ANSI_GREEN, "Here is your prize! (Ascii art)", true);
            printArt();
        } else {
            printWithColor(Colors.ANSI_RED, "You lost!", true);
            printWithColor(Colors.ANSI_RED,  "   _____                         _____                     \n" +
                    "  / ____|                       / ____|                    \n" +
                    " | |  __  __ _ _ __ ___   ___ | |     _ __ __ ___      __\n" +
                    " | | |_ |/ _` | '_ ` _ \\ / _ \\| |    | '__/ _` \\ \\ /\\ / /\n" +
                    " | |__| | (_| | | | | | | (_) | |____| | | (_| |\\ V  V / \n" +
                    "  \\_____ |\\__,_|_| |_| |_|\\___/ \\_____|_|  \\__,_| \\_/\\_/  \n" +
                    "        | |                                                 \n" +
                    "        |_|                                                 ", true);
        }
        printWithColor(Colors.ANSI_GREEN, "Total number of turns: " + stepCounter, true);
        printWithColor(Colors.ANSI_CYAN, "Play again?(yes - y, no - n)", true);
        boolean readInput = true;
        while (readInput) {
            String answer = scanner.next();
            if (answer.equalsIgnoreCase("y")) {
                readInput = false;
                InitializeGame();
                StartGame();
            } else if (answer.equalsIgnoreCase("n")) {
                readInput = false;
                System.exit(0);
            } else {
                printWithColor(Colors.ANSI_CYAN, "Say again?", true);
            }
        }
    }
}
